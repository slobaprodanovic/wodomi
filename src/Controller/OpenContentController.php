<?php

namespace Drupal\wodomi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 *
 */
class OpenContentController extends ControllerBase {
  public function homepage() {
    $tempstore = \Drupal::service('user.private_tempstore')->get('wodomi');
    $population = $tempstore->get('population_res');
    $tempstore->set('population_res', NULL);

    $out = [
      '#theme' => 'wodomi_homepage',
      '#test' => NULL,
      '#population' => $population,
      '#form' => \Drupal::formBuilder()->getForm('Drupal\wodomi\Form\ApiHttpForm'),
      '#attached' => array(
        'library' => array(
          'wodomi/wodomi_main', 'wodomi/wodomi_game'
        ),
      ),
    ];


    return $out;
  }
}
