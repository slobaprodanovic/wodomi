<?php

namespace Drupal\wodomi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;

class ApiHttpForm extends FormBase {
  protected $countries = array();
  public function getFormId() {
    return 'api_http_form';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->countries = \Drupal::service('wodomi.api_http')->getCountries();
    $form['date'] = array(
      '#type' => 'date',
      '#title' => t('Date'),
      '#required' => TRUE,
    );
    $form['gender'] = array(
      '#type' => 'select',
      '#title' => t('Gender'),
      '#options' => array(
        'female' => t('Female'),
        'male' => t('Male'),
      ),
      '#required' => TRUE,
    );
    $form['country'] = array(
      '#type' => 'select',
      '#title' => t('Country'),
      '#options' => $this->countries,
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Submit'),
      '#value' => t('Submit'),
      '#attributes' => array(
        'class' => array('text-center')
      ),
    );

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    $dob = $form_state->getValue('date');
    $gender = $form_state->getValue('gender');
    $country = $form_state->getValue('country');
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dob = $form_state->getValue('date');
    $gender = $form_state->getValue('gender');
    $country = $form_state->getValue('country');

    $countryName = $this->countries['countries'][$country];
    $countryName = $journalName = preg_replace('/\s+/', '%20', $countryName);

    $response = \Drupal::service('wodomi.api_http')->getPopulation($dob, $gender, $countryName);
    $tempstore = \Drupal::service('user.private_tempstore')->get('wodomi');
    $tempstore->set('population_res', $response->rank);
  }
}
