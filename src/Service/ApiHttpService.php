<?php

namespace Drupal\wodomi\Service;

class ApiHttpService {
  public function writeTest() {
    return 'This is a test!';
  }

  public function getPopulation($dob = '1952-03-11', $gender = 'male', $country = 'Serbia') {
    // create curl resource
    $ch = curl_init();
    // set url
//    curl_setopt($ch, CURLOPT_URL, "http://api.population.io:80/1.0/wp-rank/1952-03-11/male/United%20Kingdom/today/");
    curl_setopt($ch, CURLOPT_URL, "http://api.population.io:80/1.0/wp-rank/" . $dob . "/" . $gender . "/" . $country . "/today/");
    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output contains the output string
    $output = curl_exec($ch);
    // close curl resource to free up system resources
    curl_close($ch);

    return json_decode($output);
  }

  public function getCountries() {
    // create curl resource
    $ch = curl_init();
    // set url
    curl_setopt($ch, CURLOPT_URL, "http://api.population.io:80/1.0/countries");
    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // $output contains the output string
    $output = curl_exec($ch);
    // close curl resource to free up system resources
    curl_close($ch);

    return json_decode($output, TRUE);
  }
}
